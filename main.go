package main

import (
	"log"
	"net/http"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	errLoadingEnvFile := godotenv.Load()
	if errLoadingEnvFile != nil {
		log.Println("error loading the .env file", errLoadingEnvFile)
	}

	e := echo.New()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"}, // Allow requests from any origin.
		AllowCredentials: true,          // Allow including credentials in requests.
		AllowMethods: []string{ // Allow specified HTTP methods.
			http.MethodGet,
			http.MethodHead,
			http.MethodPut,
			http.MethodPatch,
			http.MethodPost,
			http.MethodDelete,
		},
	}))

	r := e.Group("/api/v1")
	r.GET("", func(c echo.Context) error {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"data":    "success",
			"message": "success",
		})
	})

	e.Logger.Fatal(e.Start(":14000"))
}
